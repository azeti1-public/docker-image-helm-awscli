# docker-image-helm-awscli

Since helm need AWS cli under the hood in order to get K8s cluster credentials, we need to have an image containing both tools.

Latest version is updated on each commit. It is available under :  
`docker pull registry.gitlab.com/azeti1-public/docker-image-helm-awscli`  

## Notes

We use awscli version 1.18 to workaround the problem `exec plugin is configured to use API version client.authentication.k8s.io/v1alpha1, plugin returned version client.authentication.k8s.io/v1beta1`.
